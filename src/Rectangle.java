import java.io.*;

public class Rectangle {
	Double a = 0.0, b=0.0;
	public Rectangle(){
		try {
    		System.out.println("Insert side a: "); 
    		a = Double.parseDouble(main.br.readLine());

    		System.out.println("Insert side b: "); 
    		b = Double.parseDouble(main.br.readLine());
    	} catch (IOException e){
    		e.printStackTrace();
    	}
	}
	
	public void showArea(){
		System.out.println("Area = " + a * b);
	}
	
	public void showPerimetr(){
		System.out.println("Perimetr = " + (a + b)*2);
	}
}
