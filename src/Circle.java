import java.io.*;

public class Circle {
	Double radius = 0.0;
	public Circle(){
		System.out.println("Insert radius: "); 
		try {
			radius = Double.parseDouble(main.br.readLine());
		} catch (IOException e){
			e.printStackTrace();
		}
	}
	public void showArea(){
		System.out.println("Area = " + main.PI * radius * radius);
	}
	
	public void showPerimetr(){
		System.out.println("Perimetr = " + 2 * main.PI * radius);
	}
}
