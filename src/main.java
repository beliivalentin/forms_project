import java.io.*;

public class main {
//	circle, a triangle or a rectangle
	static final double PI = 3.14159;
	static public BufferedReader br = null;
	public static void main(String[] args) {
		
		int option = 0;
		br = new BufferedReader(new InputStreamReader(System.in));
		while(true){
			System.out.println("Main menu:\n1. Circle\n2. Triangle\n3. Rectangle\n4. Exit");
			try {
				option = Integer.parseInt(br.readLine());
			} catch (IOException e) {
				e.printStackTrace();
			}
			switch (option) {
	            case 1: 
	            	Circle circle = new Circle();
	            	circle.showArea();
	            	circle.showPerimetr();
	            break;
	            case 2: 
	            	Triangle triangle = new Triangle();
	            	triangle.showArea();
	            	triangle.showPerimetr();
	            break;
	            case 3: 
	            	Rectangle rectangle = new Rectangle();
	            	rectangle.showArea();
	            	rectangle.showPerimetr();
            	break;
	            case 4: System.exit(0); break;
			}
		}
	}
}
