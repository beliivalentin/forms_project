import java.io.*;

public class Triangle {
	Double a = 0.0, b=0.0, c=0.0, p=0.0;
	public Triangle(){
		try {
    		System.out.println("Insert side a: "); 
    		a = Double.parseDouble(main.br.readLine());

    		System.out.println("Insert side b: "); 
    		b = Double.parseDouble(main.br.readLine());

    		System.out.println("Insert side c: "); 
    		c = Double.parseDouble(main.br.readLine());
    		
    		p = (a + b + c) / 2;	
    	} catch (IOException e){
    		e.printStackTrace();
    	}
	}
	
	public void showArea(){
		System.out.println("Area = " + Math.sqrt(p * (p-a) * (p-b) * (p-c)));
	}
	
	public void showPerimetr(){
		System.out.println("Perimetr = " + (a + b + c));
	}
}
